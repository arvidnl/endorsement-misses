import csv
import json
from datetime import datetime
from os import listdir, path
from os.path import isfile, join

from pprint import pp

from typing import Dict, Any

# Maps block height to gas_used
def load_indexer_stats(path: str) -> Dict[int, int]:
    results={}
    with open(path, newline='') as csv_file:
        reader = csv.reader(csv_file, delimiter=',', quotechar='|')
        for row in reader:
            if row[0] == 'row_id':
                assert row == ['row_id','height','gas_used']
                continue
            results[int(row[1])] = int(row[2])
    return results

def analyze_block(block_path: str) -> Dict[str, Any]:

    height=int(path.splitext(path.basename(block_path))[0])

    with open(block_path) as json_file:
        block_json = json.load(json_file)

        if 'blocks' not in block_json:
            print(f"Warning: Found no blocks {block_path}")
            return None

        if len(block_json["blocks"]) != 1:
            print(f"Warning: Found {len(block_json['blocks'])} blocks in {block_path}, expected 1")
            return None

        reception_time = block_json['blocks'][0]['reception_time']
        timestamp = block_json['blocks'][0]['timestamp'].replace('Z', '.000-00:00')
        validation_time = (datetime.fromisoformat(reception_time) - \
                           datetime.fromisoformat(timestamp)).total_seconds()

        if 'endorsements' not in block_json:
            print(f"Warning: Found no endorsements in {block_path}")
            return None

        endorsements=block_json['endorsements']
        endorsement_ops_expected=len(endorsements)
        endorsement_ops_received=len(
            [
                end
                for end in endorsements
                if "included_in_blocks" in end
            ]
        )
        endorsement_ops_missed=endorsement_ops_expected - endorsement_ops_received
        endorsement_ops_error=len([
            end
            for end in endorsements
            if 'errors' in end
        ])
        endorsement_ops_outdated=len([
            end
            for end in endorsements
            if "errors" in end and len([
                    end
                    for err in end["errors"]
                    if err['id'].endswith('outdated_endorsement')
            ]) > 0
        ])

        assert(endorsement_ops_received <= endorsement_ops_expected)
        assert(endorsement_ops_missed <= endorsement_ops_expected)
        assert(endorsement_ops_outdated <= endorsement_ops_error <= endorsement_ops_expected)

        return {
            'height': height,
            'hash': block_json['blocks'][0]['hash'],
            'reception_time': reception_time,
            'timestamp': timestamp,
            'validation_time': validation_time,
            'endorsement_ops_expected': endorsement_ops_expected,
            'endorsement_ops_received': endorsement_ops_received,
            'endorsement_ops_missed': endorsement_ops_missed,
            'endorsement_ops_error': endorsement_ops_error,
            'endorsement_ops_outdated': endorsement_ops_outdated
        }

def main():
    gas_used_per_block=load_indexer_stats('gas_used.csv')
    blocks_dir='blocks/'

    first_row=True
    written_rows=0
    cols=None

    with open('blocks_combined.csv', 'w', newline='') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter=',',
                                quotechar='|', quoting=csv.QUOTE_MINIMAL)
        
        files=listdir(blocks_dir)
        for block_file_basename in files:
            block_path=join(blocks_dir, block_file_basename)
            try:
                height=int(path.splitext(path.basename(block_path))[0])
            except ValueError:
                continue

            if not isfile(block_path):
                continue

            block = analyze_block(block_path)
            if block is None:
                continue

            block['gas_used'] = gas_used_per_block[height]

            if first_row:
                cols=len(block.keys())
                csv_writer.writerow(block.keys())
                first_row = False

            assert len(block.values()) == cols

            csv_writer.writerow(block.values())
            written_rows += 1

            if (written_rows % 1000 == 0):
                print(f"Written {written_rows}/{len(files)}")

    print(f"Wrote {written_rows} rows")

main()
