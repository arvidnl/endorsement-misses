This repo contains;

 - `blocks`: data on blocks, including endorsements
 - `download_gas_usage.sh`: script that downloads gas usage per block statistics from tzstats.com
 - `gas_used.csv`: the result of running `download_gas_usage.sh`
 - `combine_endorsement_data_to_csv.py`: reads the data from the
   `blocks` directory, estimates validation time, counts the number of
   missed endorsements, retrieves gas uage from `gas_Used.csv` and outputs the results to `blocks_combined.csv`
 - `blocks_combined.csv`: result of running `combine_endorsement_data_to_csv.py`
 - `plot.gnuplot`: a gnuplut that produces the following three plots from `blocks_combined.csv`:

# Plots:

![](gas-used_validation-time_missed-endorsements.png)


 - `y` axis: validation time (`reception_time` - `timestamp`).
 - `x`-axis: gas used
 - each point is colored by the number of missed endorsement operations

![](validation-time_missed-endorsements.png)

 - `y` axis: validation time (`reception_time` - `timestamp`).
 - `x`-axis: endorsement operations missed

![](gas-used_missed-endorsements.png)

 - `y` axis: gas used
 - `x`-axis: endorsement operations missed
