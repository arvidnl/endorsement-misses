#!/bin/bash

set -e

LIMIT=50000
COLS="row_id,height,gas_used"
OUTPUT_FILE=blocks.csv
CURSOR=0

MAX_ROW_ID=$(curl -s "https://api.tzstats.com/tables/block.csv?columns=$COLS&limit=1&order=desc" | tail -n1 | cut -d',' -f1)

echo $COLS > $OUTPUT_FILE

while true; do
    PERCENTAGE=$(echo "scale=2;100*$CURSOR/$MAX_ROW_ID" | bc)
    echo "$CURSOR/$MAX_ROW_ID ($PERCENTAGE %)"
    curl -s "https://api.tzstats.com/tables/block.csv?columns=$COLS&limit=$LIMIT&cursor=$CURSOR" | tail -n+2 >> $OUTPUT_FILE
    NEW_CURSOR=$(tail -n1 $OUTPUT_FILE | cut -d',' -f1)
    if [ "$NEW_CURSOR" = "$CURSOR" ]; then
        echo "Done at $CURSOR rows"
        break
    fi
    CURSOR=$NEW_CURSOR
done


