# -*- mode: gnuplot; -*-

set datafile separator ','

VALIDATION_TIME=5
ENDORSEMENT_OPS_MISSED=8
GAS_USED=11

set palette model RGB defined ( 0 '#ddff0000', 1 '#cc0000ff' )
set style circle radius 40000

set term png

set output "validation-time_missed-endorsements.png"
set xlabel "validation time"
set ylabel "endorsement ops missed"
plot 'blocks_combined.csv' using VALIDATION_TIME:ENDORSEMENT_OPS_MISSED \
     every ::1000::2000 \
     title "validation time/missed endorsements" \
     with gpoints

set output "gas-used_missed-endorsements.png"
set xlabel "gas used"
set ylabel "endorsement ops missed"
plot 'blocks_combined.csv' using GAS_USED:ENDORSEMENT_OPS_MISSED \
     every ::1000::2000 \
     title "gas used/missed endorsements" \
     with circles

set output "gas-used_validation-time_missed-endorsements.png"
set xlabel "gas used"
set ylabel "validation time"
plot 'blocks_combined.csv' using GAS_USED:VALIDATION_TIME:ENDORSEMENT_OPS_MISSED \
     every ::1000::2000 \
     title "gas used/validation time(missed endorsements)" \
     with circles palette
